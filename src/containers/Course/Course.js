import React, { Component } from 'react';

class Course extends Component {


    state = {
        courseTitle: ''
    }

    componentDidMount(){

        this.parseQueryParams();
        console.log('Course component mounted');
    }

    componentDidUpdate(){
        this.parseQueryParams();
        console.log('Course component updated');

    }

    parseQueryParams () {
        const query = new URLSearchParams(this.props.location.search);
        for(const val of query.values()){

            if(this.state.courseTitle !== val){
                this.setState({
                    courseTitle: val
                })
            }
        }
    }


    render () {

        console.log(this.props);
        return (
            <div>
                <h1>{this.state.courseTitle}</h1>
                <p>You selected the Course with ID: {this.props.match.params.courseId}</p>
            </div>
        );
    }
}

export default Course;